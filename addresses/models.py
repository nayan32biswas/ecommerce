from django.db import models
from billing.models import BillingProfile
# Create your models here.

ADDRESS_TYPES = (
    ('billing', 'Billing'),
    ('shipping', 'Shipping')
)
class AddressManager(models.Manager):
    pass

class Address(models.Model):
    billing_profile = models.ForeignKey( BillingProfile, null=True, blank=True, on_delete=models.CASCADE)
    address_type    = models.CharField(max_length=120, choices=ADDRESS_TYPES)
    address_line_1  = models.CharField(max_length=120)
    address_line_2  = models.CharField(max_length=120, null=True, blank=True)
    city            = models.CharField(max_length=120)
    country         = models.CharField(max_length=120, default="Bangladesh")
    state           = models.CharField(max_length=120)
    postal_code     = models.CharField(max_length=120)
    
    objects         = AddressManager()

    def __str__(self):
        return str(self.billing_profile)
    def get_address(self):
        return "{Line1}\n{Line2}\n{city}\n{state}, {postal}\n{country}".format(
            Line1   = self.address_line_1,
            Line2   = self.address_line_2 or "",
            city    = self.city,
            state   = self.state,
            postal  = self.postal_code,
            country = self.country
        )