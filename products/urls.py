from django.urls import path

from .views import (
    ProductListView,
    ProductDetailView,
    # ProducFeaturedtListView,
    # ProductFeaturedDetailView,
)

urlpatterns = [
    path('', ProductListView.as_view(), name="list"),
    # path('featured/', ProducFeaturedtListView.as_view()),
    path('<slug>/', ProductDetailView.as_view(), name="detail"),
    # path('featured/<slug>/', ProductFeaturedDetailView.as_view()),
]
