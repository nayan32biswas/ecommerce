from django.shortcuts import render, Http404
from django.views.generic import ListView, DetailView

from .models import Product
from carts.models import Cart


class ProductListView(ListView):
    # queryset = Product.objects.all()
    template_name = "products/list.html"

    def get_context_data(self, *args, **kwargs):
        context = super(ProductListView, self).get_context_data(
            *args, **kwargs)
        cart_obj, new_obj = Cart.objects.new_or_get(self.request)
        context["cart"] = cart_obj
        return context

    # user define queryset
    def get_queryset(self, *args, **kwargs):
        print("products views ProductDetailView get_queryset -> ", "start & end")
        # request = self.request
        return Product.objects.all()


class ProductDetailView(DetailView):
    # queryset = Product.objects.all()
    template_name = "products/detail.html"

    def get_context_data(self, *args, **kwargs):
        context = super(ProductDetailView, self).get_context_data(
            *args, **kwargs)
        cart_obj, new_obj = Cart.objects.new_or_get(self.request)
        context["cart"] = cart_obj
        return context

    # user define queryset
    def get_object(self, *args, **kwargs):
        print("products views ProductDetailView get_object -> ", "start")
        # request = self.request
        slug = self.kwargs.get("slug")
        instance = Product.objects.get_by_slug(slug)
        if instance == None:
            raise Http404("Product doesn't exist and this is a bad request")
        print("products views ProductDetailView get_object -> ", "end")
        return instance

    # def get_queryset(self, *args, **kwargs):
    #     # request = self.request
    #     pk = self.kwargs.get("pk")
    #     return Product.objects.filter(pk=pk)


# class ProducFeaturedtListView(ListView):
#     # queryset = Product.objects.featured()
#     template_name = "products/list.html"

#     def get_queryset(self, *args, **kwargs):
#         # request = self.request

#         return Product.objects.featured()


# class ProductFeaturedDetailView(DetailView):
#     queryset = Product.objects.featured()
#     template_name = "products/detail.html"
