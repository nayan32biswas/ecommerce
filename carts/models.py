from decimal import Decimal
from products.models import Product
from django.db import models
from django.conf import settings
from django.db.models.signals import pre_save, post_save, m2m_changed
User = settings.AUTH_USER_MODEL


class CartManager(models.Manager):
    def new_or_get(self, request):
        new_obj = False
        cart_id = request.session.get("cart_id", None)
        qs = self.get_queryset().filter(id=cart_id)
        if qs.exists() and qs.count() == 1:
            cart_obj = qs.first()
            if request.user.is_authenticated and cart_obj.user is None:
                cart_obj.user = request.user
                cart_obj.save()
        else:
            new_obj = True
            cart_obj = self.new(user=request.user)
            request.session["cart_id"] = cart_obj.id
        return cart_obj, new_obj

    def new(self, user=None):
        user_obj = None
        if user is not None:
            if user.is_authenticated:
                user_obj = user
        return self.model.objects.create(user=user_obj)


class Cart(models.Model):
    user = models.ForeignKey(
        User, null=True, blank=True, on_delete=models.CASCADE)
    products = models.ManyToManyField(Product, blank=True)
    total = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    subtotal = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    update = models.DateTimeField(auto_now=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    objects = CartManager()

    def __str__(self):
        return str(self.id)


def cart_m2m_changed_reveiver(sender, instance, action, *args, **kwargs):
    if action == "post_add" or action == "post_remove" or action == "post_clear":
        products = instance.products.all()
        total = 0
        for x in products:
            total += x.price
        instance.subtotal = total
        instance.save()


m2m_changed.connect(cart_m2m_changed_reveiver, sender=Cart.products.through)


def cart_presave_reveiver(sender, instance, *args, **kwargs):
    if instance.subtotal>0:
        instance.total = Decimal(instance.subtotal) * Decimal(1.08) #8% tax
    else:
        instance.total = 0


pre_save.connect(cart_presave_reveiver, sender=Cart)
