from django import forms
from django.contrib.auth import get_user_model
User = get_user_model()


class GuestForms(forms.Form):
    email = forms.EmailField()


class LoginForms(forms.Form):
    username = forms.CharField(max_length=255)
    password = forms.CharField(widget=forms.PasswordInput)


class RegisterForms(forms.Form):
    username = forms.CharField(max_length=255)
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput)
    password1 = forms.CharField(
        label='Confirm password', widget=forms.PasswordInput)

    def clean_username(self):
        username = self.cleaned_data.get("username")
        qs = User.objects.filter(username=username)
        if(qs.exists()):
            raise forms.ValidationError("Username is taken")
        return username

    def clean_email(self):
        email = self.cleaned_data.get("email")
        qs = User.objects.filter(email=email)
        if(qs.exists()):
            raise forms.ValidationError("email is taken")
        return email

    def clean(self):
        password = self.cleaned_data.get("password")
        password1 = self.cleaned_data.get("password1")
        if(password != password1):
            raise forms.ValidationError("Password must match.")
        return self.cleaned_data
