from .models import GuestEmail
from .forms import LoginForms, RegisterForms, GuestForms
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.utils.http import is_safe_url

from django.contrib.auth import get_user_model
User = get_user_model()


def guest_register_view(request):
    form = GuestForms(request.POST or None)
    context = {
        "form": form
    }
    next_get = request.GET.get("next")
    next_post = request.POST.get("next")
    redirect_path = next_get or next_post or None
    if form.is_valid():
        email = form.cleaned_data.get("email")
        new_guest_email = GuestEmail.objects.create(email=email)
        request.session['guest_email_id'] = new_guest_email.id
        if is_safe_url(redirect_path, request.get_host()):
            return redirect(redirect_path)
        else:
            return redirect("/register/")
    return redirect("/register/")


def login_view(request):
    form = LoginForms(request.POST or None)
    context = {
        "form": form
    }
    next_get = request.GET.get("next")
    next_post = request.POST.get("next")
    redirect_path = next_get or next_post or None
    if form.is_valid():
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get("password")
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            try:
                del request.session['guest_email_id']
            except:
                pass
            if is_safe_url(redirect_path, request.get_host()):
                return redirect(redirect_path)
            else:
                return redirect("/")
        else:
            print("ecommerce views login_view -> ", "if else error")
    return render(request, "accounts/login_page.html", context)


def register_view(request):
    form = RegisterForms(request.POST or None)
    print("ecommerce views -> ", request.user.is_authenticated)
    context = {
        "form": form
    }
    if form.is_valid():
        username = form.cleaned_data.get("username")
        email = form.cleaned_data.get("email")
        password = form.cleaned_data.get("password")
        new_user = User.objects.create_user(username, email, password)

    return render(request, "accounts/register_page.html", context)
