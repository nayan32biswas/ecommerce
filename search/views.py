from django.db.models import Q
from django.shortcuts import render
from django.views.generic import ListView

from products.models import Product
# Create your views here.


class SearchProductView(ListView):
    # queryset = Product.objects.all()
    template_name = "search/view.html"

    def get_context_data(self, *args, **kwargs):
        print("search views SearchProductView get_context_data -> ", "start")
        context = super(SearchProductView, self).get_context_data(
            *args, **kwargs)
        context["query"] = self.request.GET.get("q")
        print("search views SearchProductView get_context_data -> ", "end")
        return context

    # user define queryset
    def get_queryset(self, *args, **kwargs):
        print("search views SearchProductView get_queryset -> ", "start")
        request = self.request
        print("search views SearchProductView get_queryset -> get -> ", request.GET)
        query = request.GET.get("q")
        if query != None:
            return Product.objects.search(query)
        print("search views SearchProductView get_queryset -> ", "end")
        return Product.objects.featured()
