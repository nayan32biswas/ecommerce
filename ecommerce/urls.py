from django.conf import settings
from django.conf.urls.static import static

from django.contrib import admin
from django.urls import path, include
from django.contrib.auth.views import LogoutView
from addresses.views import checkout_address_create_view, checkout_address_reuse_view
from carts.views import cart_detail_api_view
from accounts.views import(
    register_view,
    login_view,
    guest_register_view
)
from .views import (
    home_view,
    contact_view,
    about_view,
)

urlpatterns = [
    path('', home_view, name="home"),
    path('about/', about_view, name="about"),
    path('contact/', contact_view, name="contact"),
    path('login/', login_view, name="login"),
    path('api/cart/', cart_detail_api_view, name="api-cart"),
    path('logout/', LogoutView.as_view(), name="logout"),
    path('register/', register_view, name="register"),
    path('checkout/address/create/', checkout_address_create_view, name="checkout_address_create"),
    path('checkout/address/reuse/', checkout_address_reuse_view, name="checkout_address_reuse"),
    path('register/guest', guest_register_view, name="guest_register"),
    path('cart/', include(('carts.urls', "carts"), namespace="cart")),
    path('products/', include(('products.urls', "products"), namespace="products")),
    path('search/', include(('search.urls', "search"), namespace="search")),
    path('admin/', admin.site.urls),
]

if settings.DEBUG:
    urlpatterns = urlpatterns + \
        static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns = urlpatterns + \
        static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
