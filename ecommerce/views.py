from django.shortcuts import render, redirect


def home_view(request):
    print("ecommerce views -> ", request.session.get("user"))
    return render(request, "home_page.html", {})


def contact_view(request):
    print("ecommerce views contact_view -> ", "start & end")
    return render(request, "contact_page.html", {})


def about_view(request):
    print("ecommerce views about_view -> ", "start & end")
    return render(request, "about_page.html", {})
