from django.db import models
from django.db.models.signals import pre_save, post_save

# user define import
from products.models import Product
from ecommerce.utils.unique_slug_generator import unique_slug_generator


class Tag(models.Model):
    title = models.CharField(max_length=255)
    slug = models.SlugField()
    timestamp = models.DateTimeField(auto_now_add=True)
    active = models.BooleanField()
    product = models.ManyToManyField(Product, blank=True)

    def __str__(self):
        return self.title


def tag_pre_save_reveiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)


pre_save.connect(tag_pre_save_reveiver, sender=Tag)
